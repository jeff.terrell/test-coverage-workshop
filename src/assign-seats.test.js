const sut = require('./assign-seats.js') // sut = "subject under test"

describe('bisect', () => {
  const items = [1, 3, 4, 7]
  const isOdd = x => x % 2 == 1
  const result = sut.bisect(items, isOdd)

  it('firstly returns the items that pass the predicate', () => {
    expect(result[0]).toEqual([1, 3, 7])
  })

  it('secondly returns the items that fail the predicate', () => {
    expect(result[1]).toEqual([4])
  })
})
